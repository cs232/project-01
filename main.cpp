/*
 * main.cpp
 *
 *  Created on: Feb 2, 2019
 *  Author: Catherine DeJager
 *  For: CS 232, Project 01 at Calvin College
 *
 *  Project 1: System Calls and Utilities
 *  Compute the average time of n repetitions of fork and/or thread creation
 */
#include <cstdlib>  // atoi
#include <iostream>
#include <string>  // string::at
#include "NanoTimer.h"  // timer class
#include <unistd.h>  // fork
#include <pthread.h>  // threads
#include <stdio.h>  // printf
using namespace std;

/*
 * Show how to use the program
 */
static void show_usage(string name) {
	std::cerr << "Usage: " << name << " <option(s)>\n"
	              << "Options:\n"
	              << "\t-rn: compute the average time of n repetitions of the system call. If this switch is omitted, then a default of 1 should be used for n.\n"
	              << "\t-p or -process: time process creation (the fork() system call).\n"
				  << "\t-t or -thread: time thread creation (the pthread_create() system call).\n"
	              << std::endl;
}

/*
 * given a thread id, say hello and then exit. This is the function that created threads run
 * Postcondition: the thread running this function is terminated
 * adapted from PrintHello method in https://www.tutorialspoint.com/cplusplus/cpp_multithreading.htm
 */
void* threadHello(void *threadid) {
	long tid;  // TODO: figure out why long
	tid = (long)threadid;
	printf("Hello from thread %ld\n", tid);
	pthread_exit(NULL);  // terminate the thread because we are done with it
}

/*
 * Compute the average time of n repetitions of fork and/or thread creation
 *
 * Command line switches:
 * -rn compute the average time of n repetitions of the system call. If this switch is omitted, then a default of 1 should be used for n.
 * -p time process creation (the fork() system call).
 * -process same as -p.
 * -t time thread creation (the pthread_create() system call).
 * -thread same as -t.
 * If none of -process, -p, -thread, nor -t are specified, your program should display a "Usage" error message and terminate.
 */
int main(int argc, char* argv[]) {
	int reps = 1;  // number of repetitions of the system call
	bool process = false;
	bool thread = false;
	NanoTimer myTimer;

	for (int i = 1; i < argc; i++) {
		string arg = string(argv[i]);
		if (arg == "-p" or arg == "-process") {
			process = true;
		}
		else if (arg == "-t" or arg == "-thread") {
			thread = true;
		}
		else if (arg.at(0) == '-' and arg.at(1) == 'r') {
			// get the rest of the argument and convert it to an integer.
			reps = atoi(arg.substr(2).c_str());
			if (reps < 1) { // invalid number of reps
				show_usage(argv[0]);
				return 1;
			}
		}
	}
	cout << endl;
	if (!process and !thread) {
		show_usage(argv[0]);
		return 1;
	}

	if (process) {
		/*
		 * Call fork() the number of times specified in rep, and time each call using myTimer.
		 * Then divide the total time across all the calls by the number of calls to get the average time.
		 */
		int pid;
		double process_time = 0;
		myTimer.reset();
		for (int i = 0; i < reps; i++) {
			myTimer.start();
			pid = fork();
			myTimer.stop();
			if (pid == 0) {
				cout << "Hello from the child process (PID = " << getpid() << ")" << endl;
				_exit(0); // child is done!
			}
			else if (pid == -1) {
				cerr << "Fork failed!" << endl;
				_exit(2);  // something went wrong, so we should exit
			}
		}
		process_time = myTimer.getTotalTime();
		double average_time = process_time / reps;
		cout << "Process total time: " << process_time << endl;
		cout << "Process average time: " << average_time << endl;
	}

	/*
	 * thread tutorials from
	 * https://www.tutorialspoint.com/cplusplus/cpp_multithreading.htm
	 * create a thread the number of times specified in rep, and time each call using myTimer.
	 * Then divide the total time across all the calls by the number of calls to get the average time.
	 */
	if (thread) {
		pthread_t threads[reps];  // create an array of posix threads with room for the number of threads we will be creating.
		int rc;  // store return code from creating the thread
		int i;
		double thread_time = 0;

		myTimer.reset();
		for (i = 0; i < reps; i++) {
			myTimer.start();
			rc = pthread_create(&threads[i], NULL, threadHello, (void *)i);
			myTimer.stop();
			if (rc) {
				cout << "Error:unable to create thread," << rc << endl;
				exit(-1);
			}
		}
		thread_time = myTimer.getTotalTime();
		double average_time = thread_time / reps;
		printf("Thread total time: %f\n", thread_time);
		printf("Thread average time: %f\n", average_time);
		/*
		 * If main() finishes before the threads it has created, and exits with pthread_exit(), the other threads will continue to execute.
		 * Otherwise, they will be automatically terminated when main() finishes.
		 * so we call pthread_exit() in main() to make sure that the other threads execute
		 */
		pthread_exit(NULL);
	}

	return 0;
}
